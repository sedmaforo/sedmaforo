
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;


entity Sem1 is
    PORT (
        LED1 : out STD_LOGIC_VECTOR(1 downto 0);
        LUZ1 : in STD_LOGIC_VECTOR(1 downto 0)
    );
end Sem1;

architecture Behavioral of Sem1 is
Signal led_int : STD_LOGIC_VECTOR(1 downto 0) := "00";
begin
LED1 <= led_int;
led_int(0) <= LUZ1(0);
led_int(1) <= LUZ1(1);
end Behavioral;
