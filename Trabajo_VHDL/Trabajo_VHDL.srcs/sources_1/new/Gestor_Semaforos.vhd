-- Entidad que traduce la informaci�n de la m�quina de estados 
-- para su uso por las entidades de los sem�foros

library IEEE;
use IEEE.STD_LOGIC_1164.ALL;


entity gestor_semaforos is
    PORT (
        Salidamaqest:in STD_LOGIC_VECTOR(4 downto 0);
        Sem1LUZ : out STD_LOGIC_VECTOR(1 downto 0);
        Sem2LUZ : out STD_LOGIC_VECTOR(2 downto 0)
    );
end gestor_semaforos;

architecture Behavioral of gestor_semaforos is
    Signal led_int : STD_LOGIC_VECTOR(1 downto 0) := "00";
    Signal led_int2 : STD_LOGIC_VECTOR(2 downto 0) := "000";
begin
    Sem1LUZ <= led_int;
    Sem2LUZ <= led_int2;
    led_int(0) <= Salidamaqest(4);
    led_int(1) <= Salidamaqest(3);
    led_int2(0) <= Salidamaqest(2);
    led_int2(1) <= Salidamaqest(1);
    led_int2(2) <= Salidamaqest(0);
end Behavioral;