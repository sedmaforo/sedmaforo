library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;

entity counter is
    generic (tiempo_espera:integer:=10);
    Port ( clk : in  STD_LOGIC;
           rst : in  STD_LOGIC;
           enable : in  STD_LOGIC;
           count : out  STD_LOGIC_VECTOR (3 downto 0);
           salida : out  STD_LOGIC);
end counter;

architecture Behavioral of counter is
signal cnt: unsigned (3 downto 0);
begin

	process (clk, rst, enable)
	variable salida_aux: std_logic;
	begin
	  if (rst='1') then
	     cnt<=(others=>'0');
		 salida<='0';
      --Para cada flanco de la senal de reloj (cada segundo), se incrementa en 1 el contador:
	  elsif clk'event and clk='1' then
			if enable ='1' then
			  if cnt<tiempo_espera then
					cnt<=cnt+1;
					salida<='0';
					--Si queda 1 para que el contaje sea igual a "tiempo_espera", es porque en
					--en este �ltimo flanco de reloj se alcanza el tiempo de espera. Por tanto,
					--la salida se pone a '1':
					if cnt=tiempo_espera-1 then
						salida<='1';
					end if;
			   else
					cnt<="0000";
					salida<='0';
				end if;
			end if;
	  end if;
	end process;
   count <=std_logic_vector(cnt);

end Behavioral;

