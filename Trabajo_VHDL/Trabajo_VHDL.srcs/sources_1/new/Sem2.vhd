

library IEEE;
use IEEE.STD_LOGIC_1164.ALL;


entity Sem2 is
    PORT (
        LED2 : out STD_LOGIC_VECTOR(2 downto 0);
        LUZ2 : in STD_LOGIC_VECTOR(2 downto 0)
    );
end Sem2;

architecture Behavioral of Sem2 is
Signal led_int : STD_LOGIC_VECTOR(2 downto 0) := "000";
begin
LED2 <= led_int;
led_int(0) <= LUZ2(0);
led_int(1) <= LUZ2(1);
led_int(2) <= LUZ2(2);
end Behavioral;
