-- Entidad sem�foro que controla las luces del sem�foro

library IEEE;
use IEEE.STD_LOGIC_1164.ALL;


entity Semaforo is
    Generic (luces: integer:=2);  -- N�mero de luces
    PORT (
        LED : out STD_LOGIC_VECTOR ( luces-1 downto 0 );
        ENTRADA : in STD_LOGIC_VECTOR  ( luces-1 downto 0 )
    );
end Semaforo;

architecture Behavioral of Semaforo is
    Signal led_int : STD_LOGIC_VECTOR( luces-1 downto 0) := (OTHERS =>'0');
    Signal count : integer := 0;
begin
        LED <= led_int;
      
        Bucle: for count in luces-1 downto 0 generate
            led_int(count) <= ENTRADA(count);
        end generate Bucle;
end Behavioral;
