-- M�quina de estados que controla las l�gica del programa

library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

ENTITY Maquina_Estados IS
    PORT (
        clk :   IN std_logic;
        reset : IN std_logic;
        Entrada1 : IN std_logic;
        Entrada2 : IN std_logic;
        Salida : OUT std_logic_vector ( 4 DOWNTO 0 )
);
END Maquina_Estados;

ARCHITECTURE behavioural OF Maquina_Estados IS
    TYPE state_type IS (S0, S1, S2);
    SIGNAL state, next_state: state_type;
    
    BEGIN
    
    SYNC_PROC: PROCESS (clk) -- Actualizador por se�al de reloj + control del reset
    BEGIN
        IF rising_edge(clk) THEN
            IF (reset = '1') THEN
                state <= S0;
            ELSE
                state <= next_state;
            END IF;
        END IF;
    END PROCESS;

    OUTPUT_DECODE: PROCESS (state) -- Asignaci�n de salidas en funci�n de los estados
    BEGIN
        CASE (state) is
            WHEN S0 =>  Salida <= "01100";
            WHEN S1 =>  Salida <= "01001";
            WHEN S2 =>  Salida <= "10010";
            WHEN OTHERS =>  Salida <= "01100";
        END CASE;
    END PROCESS;

    NEXT_STATE_DECODE: PROCESS ( Entrada1, Entrada2 ) -- Condiciones para el cambio de estado
    BEGIN
    next_state <= S0; 
        CASE (state) is 
            WHEN S0 =>
                IF (Entrada1 = '1' ) THEN
                    next_state <= S1;
                ELSIF Entrada1 = '0' THEN
                    next_state <= S0;
                END IF;
            WHEN S1 =>
                IF Entrada1 = '0' THEN
                    next_state <= S0;
                ELSIF (Entrada1 = '1' and Entrada2 = '1' ) THEN
                    next_state <= S1;
                ELSIF (Entrada1 = '1' and Entrada2 = '0' ) THEN
                    next_state <= S2;
                END IF;
            WHEN S2 =>
                IF (Entrada1 = '1' ) THEN
                    next_state <= S2;
                ELSIF ( Entrada1 = '0' ) THEN
                    next_state <= S0;
                END IF;
            WHEN OTHERS => next_state <= state;
        END CASE;
    END PROCESS;
END;