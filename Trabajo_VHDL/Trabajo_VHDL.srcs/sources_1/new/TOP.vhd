library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

entity TOP is
    Port ( 
        clk_gen, rst : IN STD_LOGIC; -- Entradas del reloj y el reset
        Sensor1, Sensor2 : IN STD_LOGIC; -- Entradas del sensor de presencia
        Verde1, Rojo1, Verde2, Rojo2, Amar2: OUT STD_LOGIC -- Salidas a las luces del sem�foro
    );
end TOP;

architecture Behavioral of TOP is

COMPONENT Maquina_Estados IS -- M�quina para el control de estados
    PORT (
        clk :   IN std_logic;
        reset : IN std_logic;
        Entrada1 : IN std_logic;
        Entrada2 : IN std_logic;
        Salida : OUT std_logic_vector ( 4 DOWNTO 0 )
);
END COMPONENT;

COMPONENT Semaforo -- Entidad que reune las luces del primer sem�foro
    GENERIC (luces: integer:=2);
    PORT (
        LED : out STD_LOGIC_VECTOR ( luces-1 downto 0 );
        ENTRADA : in STD_LOGIC_VECTOR  ( luces-1 downto 0 )
    );
END COMPONENT;

--COMPONENT Sem2 -- Entidad que reune las luces del segundo sem�foro
--    PORT (
--        LED2 : out STD_LOGIC_VECTOR(2 downto 0);
--        LUZ2 : in STD_LOGIC_VECTOR(2 downto 0)
--    );
--END COMPONENT;

COMPONENT clk_divider -- Divisor de frecuencia
    Generic ( frec: integer:=50000000);  -- Valor para 1hz: 50000000
    Port ( clk : in  STD_LOGIC;
           reset : in  STD_LOGIC;
           clk_out : out  STD_LOGIC
    );
END COMPONENT;

COMPONENT debouncer -- Eliminador de rebotes
    port (
        clk	: in std_logic;
        btn_in	: in std_logic;
        btn_out	: out std_logic
    );
END COMPONENT;

COMPONENT gestor_semaforos -- Gestor para las entidades sem�foro
    PORT (
        Salidamaqest:in STD_LOGIC_VECTOR(4 downto 0);
        Sem1LUZ : out STD_LOGIC_VECTOR(1 downto 0);
        Sem2LUZ : out STD_LOGIC_VECTOR(2 downto 0)
    );
END COMPONENT;

COMPONENT sincronizador -- Entidad sincronizadora
    Port (
        sync_in: IN STD_LOGIC;
        clk: IN STD_LOGIC;
        sync_out: OUT STD_LOGIC
    );
END COMPONENT;

signal rst_gen: STD_LOGIC; -- Se�al de reset sin rebotes
signal clk_div: STD_LOGIC; -- Se�al de reloj lenta
signal Sen1: STD_LOGIC; -- Se�al de entrada sin rebotes
signal Sen2: STD_LOGIC;
signal Sen1_fin: STD_LOGIC; -- Se�ales de entrada sincronizadas y sin rebotes
signal Sen2_fin: STD_LOGIC;
signal Salida_estados: STD_LOGIC_VECTOR ( 4 DOWNTO 0 ); -- Salida con la informaci�n para el gestor de sem�foros
signal Entrada_Sem1: STD_LOGIC_VECTOR ( 1 DOWNTO 0 ); -- Se�ales para las luces de los sem�foros
signal Entrada_Sem2: STD_LOGIC_VECTOR ( 2 DOWNTO 0 );

begin
    -- Instanciaciones de las entidades
    Debouncer_S1: debouncer
    PORT MAP (
        clk	=> clk_gen,
        btn_in => Sensor1,
        btn_out	=> Sen1
    );
    
    Debouncer_S2: debouncer
    PORT MAP (
        clk	=> clk_gen,
        btn_in => Sensor2,
        btn_out	=> Sen2
    );
    
    Debouncer_rst: debouncer
    PORT MAP (
        clk	=> clk_gen,
        btn_in => rst,
        btn_out	=> rst_gen
    );
    
    Divisor_frecuencia: clk_divider
    PORT MAP (
        clk => clk_gen,
        reset => rst_gen,
        clk_out => clk_div
    );
    
    Sincronizador_S1: sincronizador
    PORT MAP ( 
        sync_in => Sen1,
        clk => clk_div,
        sync_out => Sen1_fin
    );
    
    Sincronizador_S2: sincronizador
    PORT MAP ( 
        sync_in => Sen2,
        clk => clk_div,
        sync_out => Sen2_fin
    );

    Control_Estados: Maquina_Estados
    PORT MAP (
            clk => clk_div,
            reset => rst_gen,
            Entrada1 => Sen1_fin,
            Entrada2 => Sen2_fin,
            Salida => Salida_estados
    );

    Gestor_Salida: gestor_semaforos
    PORT MAP ( 
        Salidamaqest => Salida_estados,
        Sem1LUZ => Entrada_Sem1,
        Sem2LUZ => Entrada_Sem2
    );
    
    Semaforo1: Semaforo
    GENERIC MAP (luces => 2)
    PORT MAP (
        LED(0) => Verde1,
        LED(1) => Rojo1,
        ENTRADA => Entrada_Sem1
    );
    
    Semaforo2: Semaforo
    GENERIC MAP (luces => 3)
    PORT MAP (
        LED(0) => Verde2,
        LED(1) => Rojo2,
        LED(2) => Amar2,
        ENTRADA => Entrada_Sem2
    );

end Behavioral;
