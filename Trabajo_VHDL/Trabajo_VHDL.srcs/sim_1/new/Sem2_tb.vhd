-- Testbench para el testeo del sem�foro con 3 luces

library IEEE;
use IEEE.Std_logic_1164.all;
use IEEE.Numeric_Std.all;

entity Sem2_tb is
end;

architecture bench of Sem2_tb is

  component Sem2
  PORT (
  LED1 : out STD_LOGIC_VECTOR(2 downto 0);
  LUZ1 : in STD_LOGIC_VECTOR(2 downto 0)
  );
  end component;

  signal LED1: STD_LOGIC_VECTOR(2 downto 0);
  signal LUZ1: STD_LOGIC_VECTOR(2 downto 0) ;

begin

  uut: Sem2 port map ( LED1 => LED1,
                       LUZ1 => LUZ1 );

  stimulus: process
  begin

    LUZ1(0) <= '1';
    LUZ1(1) <= '1';
    LUZ1(2) <= '1';

    wait;
  end process;


end;
