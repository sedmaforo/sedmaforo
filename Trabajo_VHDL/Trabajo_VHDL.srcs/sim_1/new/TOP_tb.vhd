library IEEE;
use IEEE.Std_logic_1164.all;
use IEEE.Numeric_Std.all;

entity TOP_tb is
end;

architecture bench of TOP_tb is

  component TOP
      Port ( 
          clk_gen, rst : IN STD_LOGIC;
          Sensor1, Sensor2 : IN STD_LOGIC;
          Verde1, Rojo1, Verde2, Rojo2, Amar2: OUT STD_LOGIC
      );
  end component;

  signal clk_gen, rst: STD_LOGIC;
  signal Sensor1, Sensor2: STD_LOGIC;
  signal Verde1, Rojo1, Verde2, Rojo2, Amar2: STD_LOGIC ;
  
  constant clock_period: time := 10 ns;
  signal stop_the_clock: boolean;

begin

  uut: TOP port map ( clk_gen => clk_gen,
                      rst     => rst,
                      Sensor1 => Sensor1,
                      Sensor2 => Sensor2,
                      Verde1  => Verde1,
                      Rojo1   => Rojo1,
                      Verde2  => Verde2,
                      Rojo2   => Rojo2,
                      Amar2   => Amar2 );

  stimulus: process
  begin
  
    -- Put initialisation code here
    Sensor1 <= '0';
    Sensor2 <= '1';
    wait for 35000 ns;
    Sensor1 <= '1';
    wait for 35000 ns;
    Sensor1 <= '0';
    wait for 35000 ns;
    Sensor1 <= '1';
    wait for 35000 ns;
    Sensor1 <= '1';
    wait for 35000 ns;
    Sensor2 <= '0';
    wait for 35000 ns;
    Sensor2 <= '1';
    wait for 35000 ns;
    Sensor1 <= '0';
    wait for 35000 ns;

    -- Put test bench stimulus code here

    --wait;
  end process;

clocking: process
  begin
    while not stop_the_clock loop
      clk_gen <= '0', '1' after clock_period / 2;
      wait for clock_period;
    end loop;
    wait;
  end process;

end;
