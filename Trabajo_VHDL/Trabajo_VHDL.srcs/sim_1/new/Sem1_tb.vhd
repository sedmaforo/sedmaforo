-- Testbench para el testeo del sem�foro con 2 luces

library IEEE;
use IEEE.Std_logic_1164.all;
use IEEE.Numeric_Std.all;

entity Sem1_tb is
end;

architecture bench of Sem1_tb is

  component Sem1
  PORT (
  LED2 : out STD_LOGIC_VECTOR(1 downto 0);
  LUZ2 : in STD_LOGIC_VECTOR(1 downto 0)
  );
  end component;

  signal LED2: STD_LOGIC_VECTOR(1 downto 0);
  signal LUZ2: STD_LOGIC_VECTOR(1 downto 0) ;

begin

  uut: Sem1 port map ( LED2 => LED2,
                       LUZ2 => LUZ2 );

  stimulus: process
  begin
  
    LUZ2(0) <= '1';
    LUZ2(1) <= '1';

    wait;
  end process;

end;
