-- Testbench para el testeo del sincronizador

library IEEE;
use IEEE.Std_logic_1164.all;
use IEEE.Numeric_Std.all;

entity sincronizador_tb is
end;

architecture bench of sincronizador_tb is

  component sincronizador
      Port (
          sync_in: IN STD_LOGIC;
          clk: IN STD_LOGIC;
          sync_out: OUT STD_LOGIC
      );
  end component;

  signal sync_in: STD_LOGIC;
  signal clk: STD_LOGIC := '0';
  signal sync_out: STD_LOGIC ;

  constant clock_period: time := 10 ns;

begin

  uut: sincronizador port map ( sync_in  => sync_in,
                                clk      => clk,
                                sync_out => sync_out );
                                
  clk <= not clk after clock_period / 2;

  stimulus: process
  begin
    sync_in <= '0';
    wait for 5 ns;
    sync_in <= '1';
    wait for 10 ns;
    sync_in <= '0';
    wait for 12.5 ns;
    sync_in <= '1';
    wait;
  end process;

end;
