-- Testbench para el testeo de la m�quina de estados

library IEEE;
use IEEE.Std_logic_1164.all;
use IEEE.Numeric_Std.all;

entity Maquina_Estados_tb is
--  Port ( );
end;

architecture Behavioral of Maquina_Estados_tb is

  component Maquina_Estados
      PORT (
          clk :   IN std_logic;
          reset : IN std_logic;
          Sensor1 : IN std_logic;
          Sensor2 : IN std_logic;
          Sem1 : OUT std_logic_vector ( 1 DOWNTO 0 );
          Sem2 : OUT std_logic_vector ( 2 DOWNTO 0 )
  );
  end component;

  signal clk: std_logic;
  signal reset: std_logic;
  signal Sensor1: std_logic;
  signal Sensor2: std_logic;
  signal Sem1: std_logic_vector ( 1 DOWNTO 0 );
  signal Sem2: std_logic_vector ( 2 DOWNTO 0 ) ;

  constant clock_period: time := 10 ns;
  signal stop_the_clock: boolean;

begin

  uut: Maquina_Estados port map ( clk     => clk,
                                  reset   => reset,
                                  Sensor1 => Sensor1,
                                  Sensor2 => Sensor2,
                                  Sem1    => Sem1,
                                  Sem2    => Sem2 );

  stimulus: process
  begin
  
    Sensor1 <= '0';
    Sensor2 <= '1';
    wait for 35 ns;
    Sensor1 <= '1';
    wait for 35 ns;
    Sensor1 <= '0';
    wait for 35 ns;
    Sensor1 <= '1';
    wait for 35 ns;
    Sensor1 <= '1';
    wait for 35 ns;
    Sensor2 <= '0';
    wait for 35 ns;
    Sensor2 <= '1';
    wait for 35 ns;
    Sensor1 <= '0';
    wait for 35 ns;

  end process;

  clocking: process
  begin
    while not stop_the_clock loop
      clk <= '0', '1' after clock_period / 2;
      wait for clock_period;
    end loop;
    wait;
  end process;

end;