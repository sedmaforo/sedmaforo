-- Testbench para el testeo del divisor de frecuencia

LIBRARY ieee;
USE ieee.std_logic_1164.ALL;

 
ENTITY clk_divider_tb IS
END clk_divider_tb;
 
ARCHITECTURE behavior OF clk_divider_tb IS 
 
    COMPONENT clk_divider
    PORT(
         clk : IN  std_logic;
         reset : IN  std_logic;
         clk_out : OUT  std_logic
        );
    END COMPONENT;
    
   signal clk : std_logic := '0';
   signal reset : std_logic := '0';
   signal clk_out : std_logic := '0';

   constant clk_period : time := 10 ns;
 
BEGIN
 
   uut: clk_divider PORT MAP (
          clk => clk,
          reset => reset,
          clk_out => clk_out
        );

   clk_process :process
   begin
		clk <= '0';
		wait for clk_period/2;
		clk <= '1';
		wait for clk_period/2;
   end process;
 
   stim_proc: process
   begin		

      wait for 100 ns;	

		reset<='1';
		wait for 50 ns;
		
		reset<='0';

      wait;
   end process;

END;
